#define _CRT_SECURE_NO_WARNINGS 1
//191. 位1的个数
//编写一个函数，输入是一个无符号整数（以二进制串的形式），返回其二进制表达式中数字位数为 '1' 的个数（也被称为汉明重量）。
//示例 1：
//输入：n = 00000000000000000000000000001011
//输出：3
//解释：输入的二进制串 00000000000000000000000000001011 中，共有三位为 '1'。
//int hammingWeight(uint32_t n) {
//    int count = 0;
//    while (n)
//    {
//        if (n % 2 == 1)
//        {
//            count++;
//        }
//        n /= 2;
//    }
//    return count;
//
//}

//136. 只出现一次的数字
//给你一个 非空 整数数组 nums ，除了某个元素只出现一次以外，其余每个元素均出现两次。找出那个只出现了一次的元素。
//你必须设计并实现线性时间复杂度的算法来解决此问题，且该算法只使用常量额外空间。

//示例 1 ：
//输入：nums = [2, 2, 1]
//输出：1

//int singleNumber(int* nums, int numsSize) {
//    int ret = 0;
//    int i = 0;
//    for (i = 0; i < numsSize; i++)
//    {
//        ret = ret ^ nums[i];
//    }
//    return ret;
//}


//qsort函数实现数据排序
//
//#include <stdio.h>
//#include <stdlib.h>
//struct stu
//{
//	char name[20];
//	int age;
//};
//
//
//int cmp_Stu_by_age(const void* e1, const void* e2)
//{
//	return ((struct stu*)e1)->age - ((struct stu*)e2)->age;
//}
//
//
//void test3()
//{
//	struct stu arr[] = { {"zhangsan", 20}, {"lisi", 30}, {"wangwu", 12} };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), cmp_Stu_by_age);
//
//}
//
//int main()
//{
//	test3();
//	return 0;
//}

//杨氏矩阵
//有一个数字矩阵，矩阵的每行从左到右是递增的，矩阵从上到下是递增的，请编写程序在这样的矩阵中查找某个数字是否存在。
//要求：时间复杂度小于O(N);
//#include <stdio.h>
//int Find(int arr[][3], int r, int c, int k)
//{
//	int x = 0;
//	int y = c - 1;
//	while (x < r && y >= 0)
//	{
//		if (arr[x][y] < k)
//		{
//			x++;
//		}
//		else if (arr[x][y] > k)
//		{
//			y--;
//	 	}
//		else
//		{
//			return 1;
//		}
//	}
//	return 0;
//}
//
//int main()
//{
//	int arr[3][3] = { 1, 2, 3, 3, 4, 5, 6, 7, 9 };
//	int k = 7;
//	int ret = Find(arr, 3, 3, k);
//	if (ret == 1)
//	{
//		printf("找到了\n");
//	}
//	else
//	{
//		printf("找不到\n");
//	}
//
//	return 0;
//}
//
//#include <stdio.h>
//int Find(int arr[][3], int* px, int* py, int k)
//{
//	int x = 0;
//	int y = *py - 1;
//	while (x < *px && y >= 0)
//	{
//		if (arr[x][y] < k)
//		{
//			x++;
//		}
//		else if (arr[x][y] > k)
//		{
//			y--;
//		}
//		else
//		{
//			*px = x;
//			*py = y;
//			return 1;
//		}
//
//	}
//	return 0;
//}
//int main()
//{
//	int arr[3][3] = { 1, 2, 3, 3, 4, 5, 6, 7, 9 };
//	int x = 3;
//	int y = 3;
//	int k = 7;
//	int ret = Find(arr, &x, &y, k);
//	if (ret == 1)
//	{
//		printf("找到了\n");
//		printf("%d %d", x, y);
//	}
//	else
//	{
//		printf("找不到\n");
//	}
//	return 0;
//}