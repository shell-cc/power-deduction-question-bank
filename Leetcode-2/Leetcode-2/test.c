#define _CRT_SECURE_NO_WARNINGS 1


//统计位数为偶数的数字
//给你一个整数数组 nums，请你返回其中位数为 偶数 的数字的个数
//int Count(int n)
//{
//    int count = 0;
//    do
//    {
//        count++;
//        n /= 10;
//    } while (n);
//    return count;
//}
//int findNumbers(int* nums, int numsSize) {
//    int i = 0;
//    int sum = 0;
//    for (i = 0; i < numsSize; i++)
//    {
//        if (Count(nums[i]) % 2 == 0)
//        {
//            sum++;
//        }
//    }
//    return sum;
//}

//猜数字
//小A 和 小B 在玩猜数字。小B 每次从 1, 2, 3 中随机选择一个，小A 每次也从 1, 2, 3 中选择一个猜。他们一共进行三次这个游戏，请返回 小A 猜对了几次？
//输入的guess数组为 小A 每次的猜测，answer数组为 小B 每次的选择。guess和answer的长度都等于3。
//int game(int* guess, int guessSize, int* answer, int answerSize) {
//    int i = 0;
//    int sum = 0;
//    for (i = 0; i < guessSize; i++)
//    {
//        if (guess[i] == answer[i])
//        {
//            sum++;
//        }
//    }
//    return sum;
//}

// 回文数
//给你一个整数 x ，如果 x 是一个回文整数，返回 true ；否则，返回 false 。

//回文数是指正序（从左向右）和倒序（从右向左）读都是一样的整数。
//int Count(int x)
//{
//    int count = 0;
//    do
//    {
//        count++;
//        x /= 10;
//    } while (x);
//    return count;
//}
//bool isPalindrome(int x) {
//    if (x < 0)
//    {
//        return false;
//    }
//
//    int n = x;
//    int count = Count(x) - 1;
//    int ret = 0;
//    int i = 0;
//    while (x)
//    {
//        i = x % 10;
//        ret += i * pow(10, count);
//        x /= 10;
//        count--;
//
//    }
//    if (ret == n)
//    {
//        return true;
//    }
//    else
//    {
//        return false;
//    }
//
//
//}