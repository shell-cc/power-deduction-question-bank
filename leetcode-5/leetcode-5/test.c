#define _CRT_SECURE_NO_WARNINGS 1

//58. 最后一个单词的长度
//给你一个字符串 s，由若干单词组成，单词前后用一些空格字符隔开。返回字符串中 最后一个 单词的长度。
//单词 是指仅由字母组成、不包含任何空格字符的最大子字符串。
//示例 1：
//：s = "Hello World"
//：5
//：最后一个单词是“World”，长度为5。
//int lengthOfLastWord(char* s) {
//    int len = strlen(s);
//    int count = 0;
//    int i = len - 1;
//    while (i >= 0 && s[i] == ' ')
//    {
//        i--;
//    }
//    while (i >= 0 && s[i] != ' ')
//    {
//        count++;
//        i--;
//    }
//    return count;
//
//}


//#include <stdio.h>
//
//
//void reverseString(char* s, int sSize) {
//    char c = 0;
//    int i = 0;
//    for (i = 0; i < sSize / 2; i++)
//    {
//        c = s[i];
//        s[i] = s[sSize - 1 - i];
//        s[sSize - 1 - i] = c;
//    }
//    for (i = 0; i < sSize; i++)
//    {
//        printf("%c", s[i]);
//    }
//}
//
//int main()
//{
//	char s[100] = { 'h', 'e', 'l', 'l', 'o' };
//	int len = 5;
//	reverseString(s, len);
//	return 0;
//}

//283. 移动零
//给定一个数组 nums，编写一个函数将所有 0 移动到数组的末尾，同时保持非零元素的相对顺序。
//请注意 ，必须在不复制数组的情况下原地对数组进行操作
//示例 1:
//输入: nums = [0, 1, 0, 3, 12]
//输出 : [1, 3, 12, 0, 0]
//void moveZeroes(int* nums, int numsSize) {
//    int i = 0;
//    int j = 0;
//    int n = 0;
//    for (j = 0; j < numsSize; j++)
//    {
//        for (i = 0; i < numsSize - 1; i++)
//        {
//            if (nums[i] == 0)
//            {
//                n = nums[i];
//                nums[i] = nums[i + 1];
//                nums[i + 1] = n;
//            }
//        }
//    }
//}