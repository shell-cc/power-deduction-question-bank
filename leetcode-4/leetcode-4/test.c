#define _CRT_SECURE_NO_WARNINGS 1
//509. 斐波那契数
//斐波那契数 （通常用 F(n) 表示）形成的序列称为 斐波那契数列 。该数列由 0 和 1 开始，后面的每一项数字都是前面两项数字的和。也就是：
//F(0) = 0，F(1) = 1
//F(n) = F(n - 1) + F(n - 2)，其中 n > 1
//给定 n ，请计算 F(n) 。
//输入：n = 2
//输出：1
//解释：F(2) = F(1) + F(0) = 1 + 0 = 1


//int fib(int n) {
//    int a = 0;
//    int b = 1;
//    int c = 0;
//    int i = 0;
//    if (n == 0)
//    {
//        c = 0;
//    }
//    else if (n == 1)
//    {
//        c = 1;
//    }
//    else
//    {
//        for (i = 0; i < n - 1; i++)
//        {
//            c = a + b;
//            a = b;
//            b = c;
//        }
//    }
//    return c;
//
//}

//2236. 判断根结点是否等于子结点之和
//给你一个 二叉树 的根结点 root，该二叉树由恰好 3 个结点组成：根结点、左子结点和右子结点。
//如果根结点值等于两个子结点值之和，返回 true ，否则返回 false 。
//输入：root = [10,4,6]
//输出：true
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     struct TreeNode *left;
 *     struct TreeNode *right;
 * };
 */

//
//bool checkTree(struct TreeNode* root) {
//    return (root->val == root->left->val + root->right->val);
//}

//#include <stdio.h>
//#include <string.h>
//int lengthOfLastWord(char* s) {
//    int len = strlen(s);
//    int count = 0;
//    int i = len - 1;
//    while (s[i] == ' ')
//    {
//        i--;
//    }
//    while (s[i] != ' ')
//    {
//        count++;
//        i--;
//    }
//    return count;
//
//}
//
//int main()
//{
//	char *s = "hello world ";
//    int count = lengthOfLastWord(s);
//    printf("%d", count);
//	return 0;
//}a


//#include <stdio.h>
//void print(char* s)
//{
//	printf("%s", s);
//}
//int main()
//{
//	char* s = "abcdef";
//	print(s);
//	return 0;
//}

//704. 二分查找
//给定一个 n 个元素有序的（升序）整型数组 nums 和一个目标值 target  ，写一个函数搜索 nums 中的 target，如果目标值存在返回下标，否则返回 -1。
//输入: nums = [-1,0,3,5,9,12], target = 9
//输出: 4
//解释 : 9 出现在 nums 中并且下标为 4


//
//int search(int* nums, int numsSize, int target) {
//    int left = 0;
//    int right = numsSize - 1;
//    int mid = 0;
//    int k = 0;
//    while (left <= right)
//    {
//        mid = (left + right) / 2;
//        if (nums[mid] < target)
//        {
//            left = mid + 1;
//        }
//        if (nums[mid] > target)
//        {
//            right = mid - 1;
//        }
//        if (nums[mid] == target)
//        {
//            return mid;
//        }
//    }
//    return -1;
//
//}