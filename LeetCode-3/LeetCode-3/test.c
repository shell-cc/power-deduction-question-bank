#define _CRT_SECURE_NO_WARNINGS 1

//2824. 统计和小于目标的下标对数目
//给你一个下标从 0 开始长度为 n 的整数数组 nums 和一个整数 target ，请你返回满足 0 <= i < j < n 且 nums[i] + nums[j] < target 的下标对 (i, j) 的数目。
//输入：nums = [-1,1,2,3,1], target = 2
//输出：3
//
//int countPairs(int* nums, int numsSize, int target) {
//    int i = 0;
//    int j = 0;
//    int count = 0;
//    for (i = 0; i < numsSize - 1; i++)
//    {
//        for (j = i + 1; j < numsSize; j++)
//        {
//            if (nums[i] + nums[j] < target)
//            {
//                count++;
//            }
//        }
//    }
//    return count;
//
//}

//2798. 满足目标工作时长的员工数目
//公司里共有 n 名员工，按从 0 到 n - 1 编号。每个员工 i 已经在公司工作了 hours[i] 小时
//公司要求每位员工工作 至少 target 小时。
//给你一个下标从 0 开始、长度为 n 的非负整数数组 hours 和一个非负整数 target 。
//请你用整数表示并返回工作至少 target 小时的员工数。
//示例 1：
//输入：hours = [0, 1, 2, 3, 4], target = 2
//输出：3
//
//int numberOfEmployeesWhoMetTarget(int* hours, int hoursSize, int target) {
//    int i = 0;
//    int count = 0;
//    for (i = 0; i < hoursSize; i++)
//    {
//        if (hours[i] >= target)
//        {
//            count++;
//        }
//    }
//    return count;
//}