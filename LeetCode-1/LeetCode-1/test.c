#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <string.h>

int main()
{
    char s[1000] = { 0 };
    gets(s);
    int len = strlen(s);
    int i = 0;
    for (i = 0; i < len; i++)
    {
        if ('A' < s[i] && s[i] < 'Z')
        {
            s[i] = s[i] + 32;
        }
    }
    for (i = 0; i < len; i++)
    {
        printf("%c", s[i]);
    }
    return 0;
}


